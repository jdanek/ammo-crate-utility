package com.MadeByJD.JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import com.MadeByJD.JSonLibraryInterface.JSON;
import com.MadeByJD.Utils.FileGrabber;

public class Parser {
	private static Parser instance = new Parser();
	private JSONArray weaponArray = null;
	private JSONArray magezineArray = null;
	private JSONArray customsArray = null;
	private HashMap<String, ArrayList<String>> categoryArray = null;
	public static String[] typeWithSubs = {"Special","Pistol","Rifle","Launcher"};
	public static String[] typesWithExt = {"Rifle"};
	private static String[] classNameExt = {"ACO","pointer","F","ARCO","snds","GL"};
	private static String[] displayNameExt = {"ACO","Laser","Flashlight","ARCO","Silencer","Grenade Launcher"};
	
	private Parser(){
	}
	public static Parser getInstance(){
		return instance;
	}
	public void setupInstanceVariables(){
		weaponArray = JSON.parseString(FileGrabber.getInstance().getWeaponResource());
		magezineArray = JSON.parseString(FileGrabber.getInstance().getMagezineResource());
		if(!FileGrabber.getInstance().getCustomsResource().isEmpty()){
			customsArray = JSON.parseString(FileGrabber.getInstance().getCustomsResource());
		}
		categoryArray = new HashMap<String, ArrayList<String>>();
		ArrayList<String> mags = null;
		for(int x = 0; x < typeWithSubs.length; x++){
			mags = getNamesOfMagezinesFromWeapons(getNamesOfWeaponsForCategory(typeWithSubs[x]));
			categoryArray.put(typeWithSubs[x], mags);
		}
	}
	public int getNumOfWeapons(){
		return weaponArray.size();
	}
	public int getNumOfMagezines(){
		return magezineArray.size();
	}
	public int getNumOfCustoms(){
		if(customsArray != null){
			return customsArray.size();
		}
		return 0;
	}
	public ArrayList<String> getNamesOfWeapons(){
		ArrayList<String> names = new ArrayList<String>();
		
		for(int x = 0; x < getNumOfWeapons(); x++){
			JSONObject obj = (JSONObject) weaponArray.get(x);
			names.add(getDisplayNameFromJSON(obj));				
		}
		return names;
	}
	public ArrayList<String> getNamesOfWeaponsForCategory(String category){
		ArrayList<String> names = new ArrayList<String>();
		for(int x = 0; x < getNumOfWeapons(); x++){
			JSONObject obj = (JSONObject) weaponArray.get(x);

			if(obj.get("type").toString().equalsIgnoreCase(category)){
				names.add(getDisplayNameFromJSON(obj));				
			}
		}
		return names;
	}
	private String getDisplayNameFromJSON(JSONObject obj){
		String className = (String) obj.get("name");
		String displayName = (String) obj.get("displayname");
		if(!this.hasExt(obj)){
			return displayName;
		}
		for(int x = 0; x < classNameExt.length; x++){
			String ext = classNameExt[x];
			if(className.contains(ext)){
				displayName += " + " + displayNameExt[x];
			}
		}
		
		return displayName;
	}
	public ArrayList<String> getNamesOfWeaponsFromSearch(String searchText) {
		ArrayList<String> oldArray = getNamesOfWeapons();
		ArrayList<String> newArray = new ArrayList<String>();
		
		for(String s : oldArray){
			if(s.toLowerCase().contains(searchText.toLowerCase())){
				newArray.add(s);
			}
		}
		return newArray;
	}
	public ArrayList<String> getNamesOfMagezines() {
		ArrayList<String> names = new ArrayList<String>();
			
		for(int x = 0; x < getNumOfMagezines(); x++){
			JSONObject obj = (JSONObject) magezineArray.get(x);
			names.add(obj.get("name").toString());
		}
		return names;
	}
	public ArrayList<String> getNamesOfMagezinesFromWeapons(ArrayList<String> wArray) {
		ArrayList<String> output = new ArrayList<String>();
		//System.out.println(wArray);
		for(String wName : wArray){
			JSONObject wObj = getJSONFromWeaponName(wName);
			JSONArray muzzles = JSON.parseString(wObj.get("muzzles").toString());
			for(int x = 0; x < muzzles.size(); x++){
				JSONObject muzzle = (JSONObject) muzzles.get(x);
				JSONArray mags = JSON.parseString(muzzle.get("magazines").toString());
				for(Object temp : mags){
					JSONObject mag = (JSONObject) temp;
					output.add(mag.get("name").toString());
				}
			}
		}
		return output;
	}
	public ArrayList<String> getNamesOfMagezinesFromWeapon(String wName){
		ArrayList<String> temp = new ArrayList<String>();
		temp.add(wName);
		return getNamesOfMagezinesFromWeapons(temp);
	}
	private JSONObject getJSONFromMagName(String name){
		
		for(Object obj : magezineArray){
			JSONObject mag = (JSONObject) obj;
			if(mag.get("name").toString().equalsIgnoreCase(name)){
				return mag;
			}
		}
		return null;
	}
	private JSONObject getJSONFromWeaponName(String name){
		int index = name.indexOf(" +");
		if(index != -1){
			name = name.substring(0, index);
			System.out.println(name);
		}
		for(Object obj : weaponArray){
			JSONObject weapon = (JSONObject) obj;
			System.out.println(weapon);
			if(weapon.get("displayname").toString().equalsIgnoreCase(name)){
				return weapon;
			}else if (weapon.get("displayname").toString().contains(name)){
				boolean[] exts = {false,false,false,false,false,false};
				for(int x = 0; x < exts.length; x++){
					String ext = displayNameExt[x];
					if(name.contains(ext)){
						exts[x] = true;
					}
				}
				boolean correct = true;
				for(int y = 0; y < exts.length; y++){
					if(exts[y] == true){
						if(weapon.get("name").toString().contains(classNameExt[y])){
							
						}else{
							correct = false;
						}
					}
				}
				if(correct){
					return weapon;
				}
			}
		}		
		System.out.println("RETURNING NULL!");
		return null;
	}
	public ArrayList<String> getMagezinesForCategory(String name){
		return categoryArray.get(name);
	}
	public boolean hasSubTypes(String string) {
		JSONObject object = getJSONFromWeaponName(string);
		if(Arrays.asList(typeWithSubs).contains(object.get("type"))){
			return true;
		}
		return false;
	}
	private boolean hasExt(JSONObject object){
		if(Arrays.asList(typesWithExt).contains(object.get("type"))){
			return true;
		}
		return false;
	}
	
}
