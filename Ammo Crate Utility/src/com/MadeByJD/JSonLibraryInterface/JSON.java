package com.MadeByJD.JSonLibraryInterface;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONValue;

public class JSON {
	
	private JSON(){
	}
	public static JSONArray parseString(String jsonFile) {
		return (JSONArray) JSONValue.parse(jsonFile);
	}
	
}
