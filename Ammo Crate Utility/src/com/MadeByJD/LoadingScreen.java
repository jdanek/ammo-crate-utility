package com.MadeByJD;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class LoadingScreen extends JFrame {

	private JPanel contentPane;
	private JProgressBar progressBar;
	
	/**
	 * Create the frame.
	 */
	public LoadingScreen() {
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 328, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(5, 168, 318, 20);
		contentPane.add(progressBar);
		
		JLabel lblAmmoCrateUtility = new JLabel("Ammo Crate Utility");
		lblAmmoCrateUtility.setBounds(5, 5, 318, 41);
		lblAmmoCrateUtility.setHorizontalAlignment(SwingConstants.CENTER);
		lblAmmoCrateUtility.setFont(new Font("Lucida Grande", Font.PLAIN, 34));
		contentPane.add(lblAmmoCrateUtility);
		
		JLabel lblMadeByJonathan = new JLabel("Created By: Jonathan Danek");
		lblMadeByJonathan.setBounds(5, 46, 318, 41);
		lblMadeByJonathan.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblMadeByJonathan);
		
		JLabel lblWwwmadebyjdcom = new JLabel("www.MadeByJD.com");
		lblWwwmadebyjdcom.setHorizontalAlignment(SwingConstants.CENTER);
		lblWwwmadebyjdcom.setBounds(5, 70, 318, 41);
		contentPane.add(lblWwwmadebyjdcom);
		
		JLabel lblNewLabel = new JLabel("Version: 2.0");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 26));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(5, 99, 318, 57);
		contentPane.add(lblNewLabel);
		
		JLabel lblSpecialThanksDuhmedic = new JLabel("Special Thanks: DuhMedic");
		lblSpecialThanksDuhmedic.setFont(new Font("Lucida Grande", Font.PLAIN, 9));
		lblSpecialThanksDuhmedic.setHorizontalAlignment(SwingConstants.CENTER);
		lblSpecialThanksDuhmedic.setBounds(5, 147, 318, 20);
		contentPane.add(lblSpecialThanksDuhmedic);
	}
	public void setProgressTo(int p){
		progressBar.setValue(p);
	}
}
