package com.MadeByJD;

import com.MadeByJD.JSON.Parser;
import com.MadeByJD.Utils.FileGrabber;

public class MainClass {
	public static void main(String [ ] args) {
		//MainWindow window = new MainWindow();
		
		System.out.println("Running Application");
		//Parser parse = new Parser();
		final LoadingScreen load = new LoadingScreen();
		//Initialize Single Instance Classes
		load.setProgressTo(25);
		FileGrabber.getInstance().setupInstanceVariables();
		load.setProgressTo(50);
		Parser.getInstance().setupInstanceVariables();
		load.setProgressTo(75);
		try {
			Thread.sleep(2300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		load.setProgressTo(100);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		load.setVisible(false);
		MainWindow window = new MainWindow();
		window.setVisible(true);

	}
}
