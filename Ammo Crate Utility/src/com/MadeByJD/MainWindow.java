package com.MadeByJD;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;
import javax.swing.DropMode;

import com.MadeByJD.JSON.Parser;
import com.MadeByJD.Utils.FileGrabber;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JTextField SearchBar;
	private DefaultListModel weaponListModel;
	private DefaultListModel magListModel;
	private DefaultListModel crateListModel;
	private JComboBox CategorySelector;
	private JList WeaponList;
	private JList AmmoList;
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 835, 622);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmAddCustomObject = new JMenuItem("Add Custom Object");
		mnFile.add(mntmAddCustomObject);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmDevelopersWebsite = new JMenuItem("Developers Website");
		mntmDevelopersWebsite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			       try {
			           String url = "http://www.madebyjd.com";
			           System.out.println("Trying to open webpage");
			           java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
			         }
			         catch (Exception ed) {
			             System.out.println(ed.getMessage());
			         }
			}
		});
		mnHelp.add(mntmDevelopersWebsite);
		
		JMenuItem mntmDonateToDeveloper = new JMenuItem("Donate to Developer");
		mntmDonateToDeveloper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			       try {
			           String url = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y5TAM7LCXY7L6";
			           System.out.println("Trying to open webpage");
			           java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
			         }
			         catch (Exception ed) {
			             System.out.println(ed.getMessage());
			         }
			}
		});
		mnHelp.add(mntmDonateToDeveloper);
		//http://www.armaholic.com/page.php?id=21421
		JMenuItem mntmCheckForUpdates = new JMenuItem("Check For Updates");
		mntmCheckForUpdates.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			       try {
			           String url = "http://www.armaholic.com/page.php?id=21421";
			           System.out.println("Trying to open webpage");
			           java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
			         }
			         catch (Exception ed) {
			             System.out.println(ed.getMessage());
			         }
			}
		});
		mnHelp.add(mntmCheckForUpdates);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		SearchBar = new JTextField();
		SearchBar.setText("Search");
		SearchBar.setBounds(6, 12, 216, 28);
		contentPane.add(SearchBar);
		SearchBar.setColumns(10);
		SearchBar.addMouseListener(new MouseListener() {

		    public void mouseClicked(MouseEvent e) {
		    	if(SearchBar.getText().equalsIgnoreCase("Search")){
		    		SearchBar.setText("");
		    	}
		    }

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		SearchBar.addCaretListener(new CaretListener() {
	        @Override
	        public void caretUpdate(CaretEvent e) {
	        	if(!SearchBar.getText().equalsIgnoreCase("Search")){
		            System.out.println("Text Field: " + SearchBar.getText());
		            setWeaponListToSearch();	        		
	        	}
	        }
	    });

		CategorySelector = new JComboBox();
		CategorySelector.setToolTipText("Choose Weapon Category");
		CategorySelector.setBounds(6, 52, 216, 27);
		contentPane.add(CategorySelector);
		CategorySelector.addItem(new String("All"));
		CategorySelector.addItem(new String("Equip"));
		CategorySelector.addItem(new String("Launcher"));
		CategorySelector.addItem(new String("Pistol"));
		CategorySelector.addItem(new String("Rifle"));
		CategorySelector.addItem(new String("Special"));
		CategorySelector.addItem(new String("Vehicle"));
		CategorySelector.addItem(new String("Item"));
		
		CategorySelector.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        System.out.println("CategorySelector Changed to State: " + CategorySelector.getSelectedItem().toString());
		        setWeaponListToCategory(CategorySelector.getSelectedItem().toString());
		    }
		});
		JScrollPane WeaponScrollPane = new JScrollPane();
		WeaponScrollPane.setBounds(6, 112, 255, 460);
		contentPane.add(WeaponScrollPane);
		
		WeaponList = new JList();
		WeaponScrollPane.setViewportView(WeaponList);
		WeaponList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		weaponListModel = new DefaultListModel();
		WeaponList.setModel(weaponListModel);
		ArrayList<String> wArray = Parser.getInstance().getNamesOfWeapons();
		setWeaponListToArrayList(wArray);
		WeaponList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if(WeaponList.getSelectedIndex() == -1){
                	return;
                }
                if (!arg0.getValueIsAdjusting()) {
                	if(Parser.getInstance().hasSubTypes(WeaponList.getSelectedValue().toString())){
	                  setMagListToArrayList(Parser.getInstance().getNamesOfMagezinesFromWeapon(WeaponList.getSelectedValue().toString()));
                	}else{
                		setMagListToArrayList(null);
                	}
                }
            }
        });

		JScrollPane MagScrollPane = new JScrollPane();
		MagScrollPane.setBounds(273, 112, 270, 365);
		contentPane.add(MagScrollPane);
		
		AmmoList = new JList();
		AmmoList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		AmmoList.setBounds(235, 91, 211, 224);
		MagScrollPane.setViewportView(AmmoList);
		magListModel = new DefaultListModel();
		AmmoList.setModel(magListModel);
		
		JLabel lblObject = new JLabel("Object");
		lblObject.setBounds(6, 91, 61, 16);
		contentPane.add(lblObject);
		
		JLabel lblObjectAccessories = new JLabel("Ammunition (If applicable)");
		lblObjectAccessories.setBounds(273, 91, 197, 16);
		contentPane.add(lblObjectAccessories);
		
		JButton btnNewButton = new JButton("Add to Crate");
		btnNewButton.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(AmmoList.getSelectedIndex() != -1){
					if(!crateListModel.contains(AmmoList.getSelectedValue().toString())){
						crateListModel.addElement(AmmoList.getSelectedValue().toString());
					}
				}
				if(WeaponList.getSelectedIndex() != -1){
					if(!crateListModel.contains(WeaponList.getSelectedValue().toString())){
						crateListModel.addElement(WeaponList.getSelectedValue().toString());
					}
				}
			}
		});
		btnNewButton.setBounds(273, 489, 270, 83);
		contentPane.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(555, 112, 270, 365);
		contentPane.add(scrollPane);
		
		JList crateList = new JList();
		scrollPane.setViewportView(crateList);
		crateListModel = new DefaultListModel();
		crateList.setModel(crateListModel);

		JLabel lblMyAmmoCrate = new JLabel("My Ammo Crate:");
		lblMyAmmoCrate.setBounds(555, 91, 197, 16);
		contentPane.add(lblMyAmmoCrate);
		
		
		JButton btnCompile = new JButton("Compile");
		btnCompile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Opening Compile GUI");
			}
		});
		btnCompile.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		btnCompile.setBounds(555, 489, 270, 83);
		contentPane.add(btnCompile);
		ArrayList<String> mArray = Parser.getInstance().getNamesOfMagezines();
		setMagListToArrayList(mArray);
		}
	
	private void setWeaponListToCategory(String category){
		ArrayList<String> wArray = null;
		ArrayList<String> mArray = null;
		if(category.equalsIgnoreCase("All")){
			wArray = Parser.getInstance().getNamesOfWeapons();
			mArray = Parser.getInstance().getNamesOfMagezines();
		}else if(Arrays.asList(Parser.typeWithSubs).contains(category)){
			wArray = Parser.getInstance().getNamesOfWeaponsForCategory(category);
			mArray = Parser.getInstance().getMagezinesForCategory(category);
		}else{
			wArray = Parser.getInstance().getNamesOfWeaponsForCategory(category);
		}
		if(wArray != null){
			setWeaponListToArrayList(wArray);			
			setMagListToArrayList(mArray);			
		}
	}
	private void setWeaponListToSearch(){
		String searchText = SearchBar.getText();
		
		ArrayList<String> array = Parser.getInstance().getNamesOfWeaponsFromSearch(searchText);
		
		setWeaponListToArrayList(array);
	}
	private void setWeaponListToArrayList(ArrayList<String> array){
		weaponListModel.clear();
		WeaponList.setSelectedIndex(2);
		if(array != null){
			for(String s : array){
				weaponListModel.addElement(s);
			}
		}
	}
	private void setMagListToArrayList(ArrayList<String> array){
		magListModel.clear();
		if(array != null){
			for(String s : array){
				magListModel.addElement(s);
			}			
		}
	}
}