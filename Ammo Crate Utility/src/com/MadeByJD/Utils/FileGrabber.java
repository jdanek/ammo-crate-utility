package com.MadeByJD.Utils;

import java.io.IOException;

public class FileGrabber {
	private static FileGrabber instance = new FileGrabber();
	private String weaponFile;
	private String magezineFile;
	private String customsFile;
	
	private FileGrabber(){
	}
	public static FileGrabber getInstance(){
		return instance;
	}
	public void setupInstanceVariables(){
		try {
			loadFiles();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void loadFiles() throws IOException{
		weaponFile = FileLoader.readFile("WeaponResource.json");
		magezineFile = FileLoader.readFile("MagezineResource.json");
		customsFile = FileLoader.readFile("CustomsResource.json");
	}
	public String getWeaponResource(){
		return weaponFile;
	}
	public String getMagezineResource(){
		return magezineFile;
	}
	public String getCustomsResource(){
		return customsFile;
	}
}
